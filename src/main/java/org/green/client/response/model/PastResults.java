package org.green.client.response.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PastResults implements Serializable {
//    @JsonProperty("Special2")
    private String Special2;

//    @JsonProperty("Special1")
    private String Special1;

//    @JsonProperty("DrawID")
    private String DrawID;

//    @JsonProperty("ThirdPrize")
    private String ThirdPrize;

//    @JsonProperty("Special4")
    private String Special4;

//    @JsonProperty("Special3")
    private String Special3;

//    @JsonProperty("Special6")
    private String Special6;

//    @JsonProperty("FirstPrize")
    private String FirstPrize;

//    @JsonProperty("Special5")
    private String Special5;

//    @JsonProperty("Special8")
    private String Special8;

//    @JsonProperty("Special7")
    private String Special7;

//    @JsonProperty("Jackpot2Amount")
    private String Jackpot2Amount;

    private String Jackpot2Winner;

    private String Console10;

    private String Special10;

    private String PowerballJackpot1aWinner;

    private String LifeNum4;

    private String GoldJackpot2Amount;

    private String LifeNum5;

    private String LifeNum2;

    private String Jackpot1Amount;

    private String LifeNum3;

    private String LifeNum8;

    private String GoldJackpot1Winner;

    private String DrawDay;

    private String LifeNum6;

    private String PowerballJackpot1Amount;

    private String LifeNum7;

    private String LifeNum1;

    private String SecondPrize;

    private String DrawDate;

    private String PowerballJackpot1bWinner;

    private String LifePrize2Winner;

    private String DrawDayZh;

    private String PowerballJackpot2Amount;

    private String PowerballJackpot2Winner;

    private String Console1;

    private String Console2;

    private String Console3;

    private String Console4;

    private String GoldJackpot1Amount;

    private String Console5;

    private String LifeBonusNum1;

    private String Console6;

    private String LifeBonusNum2;

    private String Console7;

    private String PowerballJackpot1cWinner;

    private String Console8;

    private String Console9;

    private String Powerball2;

    private String Powerball1;

    private String LifePrize1Winner;

    private String Special9;

    private String Jackpot1Winner;

    private String GoldJackpot2Winner;

    @Override
    public String toString()
    {
        return "ClassPojo [Special2 = "+Special2+", Special1 = "+Special1+", DrawID = "+DrawID+", ThirdPrize = "+ThirdPrize+", Special4 = "+Special4+", Special3 = "+Special3+", Special6 = "+Special6+", FirstPrize = "+FirstPrize+", Special5 = "+Special5+", Special8 = "+Special8+", Special7 = "+Special7+", Jackpot2Amount = "+Jackpot2Amount+", Jackpot2Winner = "+Jackpot2Winner+", Console10 = "+Console10+", Special10 = "+Special10+", PowerballJackpot1aWinner = "+PowerballJackpot1aWinner+", LifeNum4 = "+LifeNum4+", GoldJackpot2Amount = "+GoldJackpot2Amount+", LifeNum5 = "+LifeNum5+", LifeNum2 = "+LifeNum2+", Jackpot1Amount = "+Jackpot1Amount+", LifeNum3 = "+LifeNum3+", LifeNum8 = "+LifeNum8+", GoldJackpot1Winner = "+GoldJackpot1Winner+", DrawDay = "+DrawDay+", LifeNum6 = "+LifeNum6+", PowerballJackpot1Amount = "+PowerballJackpot1Amount+", LifeNum7 = "+LifeNum7+", LifeNum1 = "+LifeNum1+", SecondPrize = "+SecondPrize+", DrawDate = "+DrawDate+", PowerballJackpot1bWinner = "+PowerballJackpot1bWinner+", LifePrize2Winner = "+LifePrize2Winner+", DrawDayZh = "+DrawDayZh+", PowerballJackpot2Amount = "+PowerballJackpot2Amount+", PowerballJackpot2Winner = "+PowerballJackpot2Winner+", Console1 = "+Console1+", Console2 = "+Console2+", Console3 = "+Console3+", Console4 = "+Console4+", GoldJackpot1Amount = "+GoldJackpot1Amount+", Console5 = "+Console5+", LifeBonusNum1 = "+LifeBonusNum1+", Console6 = "+Console6+", LifeBonusNum2 = "+LifeBonusNum2+", Console7 = "+Console7+", PowerballJackpot1cWinner = "+PowerballJackpot1cWinner+", Console8 = "+Console8+", Console9 = "+Console9+", Powerball2 = "+Powerball2+", Powerball1 = "+Powerball1+", LifePrize1Winner = "+LifePrize1Winner+", Special9 = "+Special9+", Jackpot1Winner = "+Jackpot1Winner+", GoldJackpot2Winner = "+GoldJackpot2Winner+"]";
    }
}


