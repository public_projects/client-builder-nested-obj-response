package org.green.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.green.client.config.MapConfig;
import org.green.client.convertor.DataConvertor;
import org.green.client.response.model.PastResultContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PublicApiClient implements CommandLineRunner {
    @Autowired
    private WebClient.Builder webClientBuilder;
    @Autowired
    private MapConfig mapConfig;
    @Autowired
    private List<DataConvertor> dataConvertors;

    public Flux<String> getRestData1(String api) {
        log.info("URL: {}", api);
        return webClientBuilder.build().get().uri(api)
                .retrieve()
                .bodyToFlux(String.class)
                .retryBackoff(5, Duration.ofSeconds(1), Duration.ofSeconds(20))
                .doOnError(IOException.class, e -> log.error(e.getMessage()));
    }

    public Flux<PastResultContainer> getRestData2(String api) {
        log.info("URL: {}", api);
        return webClientBuilder.build().get().uri(api)
                .retrieve()
                .bodyToFlux(PastResultContainer.class)
                .retryBackoff(5, Duration.ofSeconds(1), Duration.ofSeconds(20))
                .doOnError(IOException.class, e -> log.error(e.getMessage()));
    }

    public void startProcess(PastResultContainer pastResultContainer) {
        dataConvertors.forEach(dataConvertor -> {
            try{
                dataConvertor.startProcess(pastResultContainer);
            } catch (Exception ex){
                log.error("Failed to run convertor", ex);
            }

        });
    }

    public void endProcess() {
        dataConvertors.forEach(dataConvertor -> {
            try{
                dataConvertor.close();
            } catch (Exception ex){
                log.error("Failed to close convertor", ex);
            }

        });
    }

    @Override
    public void run(String... args) throws Exception {
        PastResultContainer pastResultContainer = null;
        for(String api: mapConfig.getAPIs()){
            log.info("String value: {}", getRestData1(api).blockFirst());
            pastResultContainer = getRestData2(api).blockFirst();
            log.info("Object value: {}", pastResultContainer.getPastResultsRange());
            startProcess(pastResultContainer);
        }

        endProcess();
    }
}
