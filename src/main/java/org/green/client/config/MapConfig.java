package org.green.client.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "map.config")
@Getter
@Setter
public class MapConfig {
    List<ConvertorConfig> convertors = new ArrayList<>();
    List<String> APIs = new ArrayList<>();
}
