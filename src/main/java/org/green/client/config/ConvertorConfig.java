package org.green.client.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class ConvertorConfig {
    private String bean;
    private String locationToSaveFile;
}
