package org.green.client;

import lombok.extern.slf4j.Slf4j;
import org.green.client.config.MapConfig;
import org.green.client.convertor.DataConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootApplication
public class ClientBuilderNestedObjResponseApplication {
	@Autowired
	private MapConfig mapConfig;

	@Autowired
	private ApplicationContext context;

	private List<DataConvertor> dataConvertors  = new ArrayList<>();

	@Bean
	public WebClient.Builder getWeClientBuilder(){
		return WebClient.builder();
	}

	@Bean(name = "requestDateFormat")
	public DateTimeFormatter getRequestDateFormat(){
		return DateTimeFormatter.ofPattern("dd-MM-yyyy");
	}

	@Bean
	public List<DataConvertor> getDataConvertor(){
		return dataConvertors;
	}

	@PostConstruct
	public void init(){
		mapConfig.getConvertors().forEach(convertorName -> {
			log.info("{}", convertorName.toString());

			DataConvertor dataConvertor = (DataConvertor) context.getBean(convertorName.getBean(), convertorName.getLocationToSaveFile());

			log.info("class name: {}", dataConvertor.getClass().getSimpleName());
		});
	}

	public static void main(String[] args) {
		SpringApplication.run(ClientBuilderNestedObjResponseApplication.class, args);
	}
}
