package org.green.client.convertor.impl;

import lombok.extern.slf4j.Slf4j;
import org.green.client.convertor.DataConvertor;
import org.green.client.response.model.PastResultContainer;
import org.green.client.response.model.PastResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Component(value = "CsvConvertor")
public class CsvConvertor implements DataConvertor {
    private boolean isEmpty = true;

    private File directoryToSaveFile;

    private String[] fieldNames;

    private Writer out;

    @Autowired
    private DateTimeFormatter requestDateFormat;

    public CsvConvertor(String locationToSaveFile) {
        this.directoryToSaveFile = new File(locationToSaveFile);
    }

    @Override
    public String startProcess(PastResultContainer pastResultContainer) throws IOException {
        PastResults pastResults = pastResultContainer.getPastResultsRange().getPastResults();
        createOutputFile();

        log.info("Directory {}, exist: {}, isEmpty: {}",
                directoryToSaveFile, directoryToSaveFile.exists(),
                isEmpty);


        if (isEmpty) {
            createNewFile(pastResults);
        } else {

            if(fieldNames == null){
                log.error("Deleted previously generated data file and try again!");
                System.exit(1);
            }

            processExistingFile(pastResults);
        }

        return null;
    }

    private void createOutputFile() throws IOException {
        isEmpty = DataConvertor.isEmpty(directoryToSaveFile);

        if (isEmpty) {
            String fileSeparator = System.getProperty("file.separator");
            String fileName = LocalDateTime.now().format(requestDateFormat) + ".csv";
            log.info("New resultFileName: {}", fileName);
            String absoluteFilePath = directoryToSaveFile.getAbsolutePath() + fileSeparator + fileName;
            File file = new File(absoluteFilePath);
            if (file.createNewFile()) {
                log.info(" {} File Created", absoluteFilePath);
                this.directoryToSaveFile = file;
                this.out = new OutputStreamWriter(new FileOutputStream(directoryToSaveFile), "UTF8");
            }
        }
    }

    private void createNewFile(PastResults pastResults) {
        Field[] fields = PastResults.class.getDeclaredFields();

        StringBuilder cvsHeader = new StringBuilder();
        StringBuilder cvsValues = new StringBuilder();

        this.fieldNames = new String[fields.length];

        try {
            for (int i = 0; i < fields.length; i++) {
                this.fieldNames[i] = fields[i].getName();
                cvsHeader.append(this.fieldNames[i]).append(",");
                String value = (String) new PropertyDescriptor(this.fieldNames[i], PastResults.class).getReadMethod().invoke(pastResults);
                cvsValues.append(value).append(",");
//                log.info("field name: {}, value: {}", this.fieldNames[i], value);
            }

            String csvFileValue = cvsHeader.substring(0, cvsHeader.length() - 1) + System.lineSeparator() + cvsValues.substring(0, cvsValues.length() - 1);
            out.write(csvFileValue);
            log.info("fileContent: {}", csvFileValue);
        } catch (IOException ex) {
            log.error("failed to write to file ", ex);
        } catch (Exception ex) {
            log.error("failed to call getter method: ", ex);
        }
        this.isEmpty = false;
    }

    private void processExistingFile(PastResults pastResults) {
        StringBuilder cvsValues = new StringBuilder();



        try {
            for (String field : fieldNames) {
                String value = (String) new PropertyDescriptor(field, PastResults.class).getReadMethod().invoke(pastResults);
                cvsValues.append(value).append(",");
            }

            String csvFileValue = System.lineSeparator() + cvsValues.substring(0, cvsValues.length() - 1);
            out.write(csvFileValue);
            log.info("fileContent: {}", csvFileValue);
        } catch (IOException ex) {
            log.error("failed to write to file ", ex);
        } catch (Exception ex) {
            log.error("failed to call getter method: ", ex);
        }
    }

    @Override
    public void close() throws IOException {
        out.close();
    }
}
