package org.green.client.convertor;

import org.green.client.response.model.PastResultContainer;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public interface DataConvertor extends Serializable {
    String startProcess(PastResultContainer pastResultContainer) throws IOException;

    void close() throws IOException;

    static boolean isEmpty(File directory){
        try{
            try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(directory.getAbsolutePath()))) {
                return !dirStream.iterator().hasNext();
            }
        }catch(Exception ex){
            // Don't care!
        }
        return false;
    }
}
