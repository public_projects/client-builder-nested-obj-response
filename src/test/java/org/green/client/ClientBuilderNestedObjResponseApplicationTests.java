package org.green.client;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;

@Slf4j
@SpringBootTest
class ClientBuilderNestedObjResponseApplicationTests {

    @Test
    void contextLoads() throws Exception {
        File directoryToSaveFile = new File("/home/greenhon/Documents/development/client-builder-nested-obj-response/csv-files/30-12-2019-1.csv");
//        FileOutputStream os = new FileOutputStream(directoryToSaveFile);
        String csvFileValue = "7179,3958,276/19,2472,3576,7981,3827,4050,7378,1803,5122,245761.33,1.3080400754,6784,1067,,16,142603.04,17,08,7091555.78,09,29,0.0000,TUE,23,,24,03,8178,03/12/2019,,1.0000000000,星期二,,,8320,2356,6555,8860,3110226.94,1623,25,2337,28,8913,,1761,2407,,,0.0000000000,9728,0.0555264536,0.0500";
//        String csvFileValue = "7179,3958,276/19,2472,3576,7981,3827,4050,7378,1803,5122,245761.33,1.3080400754,6784,1067,,16,142603.04,17,08,7091555.78,09,29,0.0000,TUE,23,,24,03,8178,03/12/2019,,1.0000000000,,,8320,2356,6555,8860,3110226.94,1623,25,2337,28,8913,,1761,2407,,,0.0000000000,9728,0.0555264536,0.0500,星期二";
//        log.info("lenght of input string: {}", csvFileValue.length());
//
//        os.write(csvFileValue.getBytes("Unicode"), 0, csvFileValue.length());
//
//        os.close();

        try {
            FileOutputStream fos = new FileOutputStream(directoryToSaveFile);
            Writer out = new OutputStreamWriter(fos, "UTF8");
            out.write(csvFileValue);
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
